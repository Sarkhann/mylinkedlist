public class Test {
    public static void main(String[] args) {
        LinkedList<Integer> list = new LinkedList<>();
        list.addHead(23);
        list.addTail(2);
        list.addTail(3);
        list.addTail(9);
        list.addTail(11);
        list.addTail(15);
        list.addTail(33);
        System.out.println(list);
        System.out.println("Size: " + list.getSize()); // Size: 3
        System.out.println("First: " + list.get(0)); // First: 1
        System.out.println("Last: " + list.get(list.getSize() - 1)); // Last: 3
        System.out.println("Element at index 1: " + list.get(1)); // Element at index 1: 2
        System.out.println(list);
        System.out.println("Removing element at index 1: " + list.removeIndex(1)); // Removing element at index 1: 2
        System.out.println(list);
        System.out.println("Size after removal: " + list.getSize()); // Size after removal: 2
        System.out.println(list);
        System.out.println("Before removing:");
        System.out.println("List: " + list);
        System.out.println("Size: " + list.getSize());
        list.removeElement(99);
        System.out.println("After removing:");
        System.out.println("List: " + list);
        System.out.println("Size: " + list.getSize());
        System.out.println("");
        System.out.println(list.getFirst());
        System.out.println(list.getLast());
        System.out.println("");
        System.out.println(list.indexOf(15));
        System.out.println(list.indexOf(95));
        System.out.println("");
        System.out.println(list.contains(33));
        System.out.println(list.contains(83));
    }
}
