import java.util.*;

public class LinkedList<T> implements List {
    private int size;
    private Node<T> first;
    private Node<T> last;

    public LinkedList() {
        this.size = 0;
        this.first = null;
        this.last = null;
    }

    public void addTail(T tail) {
        Node<T> newNode = new Node<>(tail);
        if (first == null) {
            first = newNode;
            last = newNode;
        } else {
            last.setNext(newNode);
            newNode.setPrev(last);
            last = newNode;
        }
        size++;
    }

    public void addHead(T head) {
        Node<T> newNode = new Node<>(head);
        if (first == null) {
            first = newNode;
            last = newNode;
        } else {
            newNode.setNext(first);
            first.setPrev(newNode);
            first = newNode;
        }
        size++;
    }

    public boolean removeElement(T element) {
        Node<T> removeElement = first;
        while (removeElement != null) {
            if (removeElement.getData().equals(element)) {
                Node<T> prev = removeElement.getPrev();
                Node<T> next = removeElement.getNext();
                if (prev != null || next != null) {
                    prev.setNext(next);
                    next.setPrev(prev);
                } else {
                    first = next;
                    last = prev;
                }
                size--;
                return true;
            }
            removeElement = removeElement.getNext();
        }
        return false;
    }

    public int getSize() {
        return size;
    }


    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        if (this.first == null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean contains(Object o) {
        Node<T> findElement = first;
        while (findElement != null) {
            if (findElement.getData().equals(o)) {
                return true;
            }
            findElement = findElement.getNext();
        }
        return false;
    }

    @Override
    public Iterator iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        Object[] arr = new Object[size];
        Node<T> elements = first;
        for (int i = 0; i < size; i++) {
            arr[i] = elements.getData();
            elements = elements.getNext();
        }
        return arr;
    }

    @Override
    public boolean add(Object object) {
        return false;
    }

    @Override
    public boolean remove(Object o) {
        Node<T> removeElement = first;
        while (removeElement != null) {
            if (removeElement.getData().equals(o)) {
                Node<T> prev = removeElement.getPrev();
                Node<T> next = removeElement.getNext();
                if (prev != null || next != null) {
                    prev.setNext(next);
                    next.setPrev(prev);
                } else {
                    first = next;
                    last = prev;
                }
                size--;
                return true;
            }
            removeElement = removeElement.getNext();
        }
        return false;
    }

    @Override
    public boolean addAll(Collection c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection c) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public Object get(int index) {
        if (index < 0 || index > size) {
            throw new IndexOutOfBoundsException("Index " + index + " is out of bounds; Size of the list is " + size);
        }
        Node<T> findElement = first;
        int count = 0;
        while (count < index) {
            findElement = findElement.getNext();
            count++;
        }
        return findElement.getData();
    }

    @Override
    public Object set(int index, Object element) {
        return null;
    }

    @Override
    public void add(int index, Object element) {

    }

    @Override
    public Object remove(int index) {
        if (index < 0 || index > size) {
            throw new IndexOutOfBoundsException("Index " + index + " is out of bounds; Size of the list is " + size);
        } else if (first == null) {
            return null;
        } else if (index == size - 1) {
            size--;
            return remove(size - 1);
        } else {
            Node<T> removeIndex = first;
            for (int i = 0; i < index; i++) {
                removeIndex = removeIndex.getNext();
            }
            Node<T> prev = removeIndex.getPrev();
            Node<T> next = removeIndex.getNext();

            if (prev != null || next != null) {
                prev.setNext(next);
                next.setPrev(prev);
            } else {
                first = next;
                last = prev;
            }
            removeIndex.setData(null);
            size--;
            return removeIndex.getData();
        }
    }

    @Override
    public int indexOf(Object o) {
        Node<T> findElement = first;
        int index = 0;
        while (findElement != null) {
            if (findElement.getData().equals(o)) {
                return index;
            }
            findElement = findElement.getNext();
            index++;
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator listIterator() {
        return null;
    }

    @Override
    public ListIterator listIterator(int index) {
        return null;
    }

    @Override
    public List subList(int fromIndex, int toIndex) {
        return null;
    }

    @Override
    public boolean retainAll(Collection c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection c) {
        return false;
    }

    @Override
    public boolean containsAll(Collection c) {
        return false;
    }

    @Override
    public Object[] toArray(Object[] a) {
        return new Object[0];
    }


    public T getFirst() {
        if (first == null) {
            throw new NoSuchElementException("List is empty");
        }
        return first.getData();
    }

    public T getLast() {
        if (last == null) {
            throw new NoSuchElementException("List is empty");
        }
        return last.getData();
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        Node<T> elements = first;
        while (elements != null) {
            stringBuilder.append(elements.getData() + " ");
            elements = elements.getNext();
        }
        return stringBuilder.toString();
    }


}
